#testcase for sep ass3
#author: Christoph Maurer
#https://palme.iicm.tugraz.at/wiki/SEP/Ass3_SS12

import os
import sys

def check_file(file,exit_code,memcheck,valgrind_error,files):
	rv=False
	sysrv=0
	cmd="./ass3"
	outfile="ass3_%02d.out"%(files)
	sysrv=os.system(cmd+file+">"+outfile)
	reffile="ass3_%02d.ref"%(files)
	fp_out=open(outfile,"r")
	fp_out_content=fp_out.read()
	fp_out.close()
	fp_ref=open(reffile,"r")
	fp_ref_content=fp_ref.read()
	fp_ref.close()
	
	if fp_ref_content!=fp_out_content:
		rv=False
		status="FAILED"
	else:
		rv=True
		status="OK"
	
	print "compare %s with %s (input=%s)\t\t[%s]"%(reffile,outfile,file,status)
		
	if memcheck==True:
		valgrindrv=os.system("valgrind --tool=memcheck --leak-check=yes --error-exitcode=1 ./ass3"+file)
		if valgrindrv==valgrind_error:
			rv=False
			status="FAILED"
		else:
			status="OK"
		print "memcheck (input=%s)\t\t[%s]"%(file,status)
	
	return rv

def create_ref(file,files):
	print "processing file %s"%(file)
	reffile="ass3_%02d.ref"%(files)
	os.system("./ass3"+ file + ">" + reffile)
	

def main():
	errors=0
	files=0
	ok=0
	memcheck=False
	exit_code=0
	createref=False
	
	if len(sys.argv)==2 and sys.argv[1]=="-memcheck":
		memcheck=True
	elif len(sys.argv)==2 and sys.argv[1]=="-cref":
			createref=True
	elif len(sys.argv)==3 and sys.argv[1]=="-exit":
		sys.exit(int(sys.argv[2]))
	
	valgrind_error=os.system("python sep_ass3_test.py -exit 1");
	
	test_data = ["<ass3_01.txt","<ass3_02.txt","<ass3_03.txt","<ass3_04.txt","<ass3_05.txt","<ass3_06.txt"
							 ,"<ass3_07.txt Ra1Kb1kh8Qa2Qa3Qb6",
								"<ass3_08.txt Ra1Kb1kh8Qa2Qa3Qb6",
								"<ass3_09.txt",
"<ass3_10.txt Ra1Nb1Bc1Qd1Ke1Bf1Ng1Rh1Pb2Pc2Pd2Pe2Pg2Ph2Pa3Pf3pb5pa7pc7pd7pe7pf7pg7ph7ra8nb8bc8qd8ke8bf8ng8rX8","<ass3_11.txt Ra1Nb1Bc1Qd1Ke1Bf1Ng1Rh1Pb2Pc2Pd2Pe2Pg","<ass3_12.txt Ra1 Nb1"]


	if createref == False:
		os.system("rm *.out")
		for data in test_data:
			files+=1
				
			if check_file(data,exit_code,memcheck,valgrind_error,files)==False:
				errors +=1
			else:
				ok+=1
				
		print "\nRESULT:\n"
		print "Inputfiles:\t"+str(files)
		print "OK:\t"+str(ok)
		print "ERRORS:\t"+str(errors)
		
		if errors>0 and files>0:
			test_status="failed!"
		else:
			test_status="ok!"
		print "Test "+test_status
	else:
		for data in test_data:
			files+=1			
			create_ref(data,files)			
		print "%d .ref-Files created"%(files)
	
if __name__ == "__main__":
	main()

