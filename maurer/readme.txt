Verwendung: python sep_ass3_test.py bzw. sep_ass3_test.py -memcheck

-------------------------------------------------------------------

ass3_01.txt: einfacher Test mit normaler Startaufstellung

ass3_02.txt: einfache move commands

ass3_03.txt: schnellst mögliches Schachmatt (Narrenmatt)

ass3_04.txt: draw command

ass3_05.txt: setname command

ass3_06.txt: viele move commands, Figuren werden geschlagen

ass3_07.txt: Patt-Situation config=Ra1Kb1kh8Qa2Qa3Qb6 Stalemate - the game ends in a draw.

ass3_08.txt: Schachmatt config=Ra1Kb1kh8Qa2Qa3Qb6

ass3_09.txt: history command

ass3_10.txt: Invalid setup string: config=Ra1Nb1Bc1Qd1Ke1Bf1Ng1Rh1Pb2Pc2Pd2Pe2Pg2Ph2Pa3Pf3pb5pa7pc7pd7pe7pf7pg7ph7ra8nb8bc8qd8ke8bf8ng8rX8

ass3_11.txt: Corrupt setup string config=Ra1Nb1Bc1Qd1Ke1Bf1Ng1Rh1Pb2Pc2Pd2Pe2Pg

ass3_12.txt: Invalid Parameter count.: ./ass3 Ra1 Nb1

-------------------------------------------------------------------

Viel Erfolg beim Testen!
Christoph Maurer
c.maurer@student.tugraz.at

