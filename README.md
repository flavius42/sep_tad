This serves as a good starting point for testing and semi-automatization
purposes for *LV 706.007*.

Getting started
===============

1. fork your own copy from https://gist.github.com/2012324
2. `git clone <that copy's private clone URL> framework` side-by-side with your normal repository
   so you will end up with two directories `uni/sep/project/` and `uni/sep/framework/`
3. `cd framework; git submodule init; git submodule update`
4. create a symlink from `uni/sep/framework/src/` to `uni/sep/project/<src>` - `<src>` is where your
  `.cpp` and `.h` files lie, in your private repository (subversion or git) that noone will see.

Make Targets
============

- `make`
  - just compile the program
- `make tests`
  - compile the program if necessary, then run the tests on it
- `make deliverable`
  - create archive (ready for abgabe, DO NOT FORGET to update the `DELIVERABLES` variable in Makefile)
  - run the tests beforehand => IMPOSSIBLE to hand in something that doesn't pass the tests
- `make format`
  - run astyle over the source code (the `DELIVERABLES` variable) - not guaranteed to be perfect, but Weber-proof in the scope of `astyle` at least

Testing unit
============

Add your tests in `uni/sep/framework/tests/`. The documentation is at http://cxxtest.com/cxxtest/doc/guide.html.

Commit and push your tests, so anyone can benefit from them.

???
===

- fun
- profit (ECTS)
