// PlayerFullNames.h
#include <cxxtest/TestSuite.h>
#include <cxxtest/GlobalFixture.h>

#include <src/Player.h>
#include <string>
using Base::Player;

class Fixture1 : public CxxTest::GlobalFixture
{
public:
    unsigned setUpCount;
    unsigned tearDownCount;

    Fixture1() { setUpCount = tearDownCount = 0; }

    bool setUp() { return true; }
    bool tearDown() { return true; }

    bool setUpWorld() { setUpCount++; return true;}
    bool tearDownWorld() { tearDownCount++; return true;}
};
static Fixture1 fixture1;

/** CRAPPY SPECIFICATION DOES NOT ALLOW US TO DO IT PROPERLY
enum _Test_Color
{
  BLACK, WHITE, RED, GREEN, BLUE, YELLOW, ORANGE, BROWN
};
*/

static const std::string _test_color_names[] = { "Black", "White", "Red", "Green",
    "Blue", "Yellow", "Orange", "Brown"};

static const std::string _test_player_names[] = { "Foo Black", "Bar White",
    "Red Moon", "Green World", "Blue Screen of Death", "Yellow Submarine",
    "Orange X", "Brown ...haha, da sag ich nichts"};

class PlayerFullNames : public CxxTest::TestSuite
{
    Player *player;
    std::string expected;
    Color color;
public:
    void setUp() {
        unsigned cnt = fixture1.setUpCount-1;
        color = static_cast<Color>(cnt);
        player = new Player(color, _test_player_names[cnt]);
        expected = _test_player_names[cnt] + "(" + _test_color_names[cnt] + ")";
    }
    void tearDown() {
        delete player;
    }

    void test_BlackPlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_WhitePlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_RedPlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_GreenPlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_BluePlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_YellowPlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_OrangePlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
    void test_BrownPlayer(void)
    {
        TS_ASSERT_EQUALS(expected, player->getFullName());
    }
};

