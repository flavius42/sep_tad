// PlayerCopyCtor.h
#include <cxxtest/TestSuite.h>

#include <src/Player.h>
#include <string>

class PlayerCopyCtor : public CxxTest::TestSuite
{
public:
    void setUp() {}
    void tearDown() {}
    void test_CopyCtor() {
        Player foo(YELLOW, "Foo");
        Player bar=foo;
        bar.setColor(BLACK);
        bar.setName("Bar");

        TS_ASSERT_EQUALS("Foo(Yellow)", foo.getFullName());
        TS_ASSERT_EQUALS("Bar(Black)", bar.getFullName());
    }
};

