// PlayerName.h
#include <cxxtest/TestSuite.h>

#include <src/Player.h>
#include <string>

class PlayerName : public CxxTest::TestSuite
{
    Player* player;
public:
    void setUp() {
        player = new Player(BLACK, "Foo");
    }
    void tearDown() {
        delete player;
    }
    void test_NormalName() {
        TS_ASSERT_EQUALS("Foo", player->getName());
    }
    void test_SetName() {
        player->setName("Bar");
        TS_ASSERT_EQUALS("Bar", player->getName());
    }
};

