#include <cxxtest/TestSuite.h>

#include <src/Game.h>
#include <src/Player.h>
#include <src/Point.h>

#include <string>
#include <sstream>
#include <fstream>
#include <streambuf>

using namespace Base;

using namespace std;

class OfficialTests : public CxxTest::TestSuite
{
    Game* game;
    stringstream *input;
    stringstream *output;
    streambuf* cout_backup, *cin_backup;
    string run_output;
public:
    void setUp() {
        run_output = "";
        cout_backup = cout.rdbuf();
        cin_backup = cin.rdbuf();
        input = new stringstream;
        output = new stringstream;
        cin.rdbuf(input->rdbuf());
        cout.rdbuf(output->rdbuf());
    }
    void tearDown() {
        cin.rdbuf(cin_backup);
        cout.rdbuf(cout_backup);
        delete input;
        delete output;
        /*
        cout << endl;
        cout << "--------------------" << endl;
        cout << run_output;
        cout << "--------------------" << endl;
        */
    }

    void test_Quit() {
        run_standard_diff_file("ass2_01");
    }
    void test_CaseInsensitive_Command_Quit() {
        run_standard_diff_file("ass2_02");
    }
    void test_CaseInsensitive_Command_Status() {
        run_standard_diff_file("ass2_03");
    }
    void test_Command_Echo() {
        run_standard_diff_file("ass2_04");
    }
    void test_Command_View() {
        run_standard_diff_file("ass2_05");
    }
    void test_Garbage_Commands() {
        run_standard_diff_file("ass2_06");
    }
    void test_Space_After_Echo() {
        run_standard_diff_file("ass2_07");
    }
    void run_standard_diff_file(string name) {
        game = getFixture();
        *input << getCommands(name);
        game->run();
        string buffer;
        getline(*output, buffer, '\0');
        string expected = getExpected(name);
        TS_ASSERT_EQUALS(expected, buffer);
        delete game;
    }
    Game *getFixture() {
        Point bounds = {5,5};
        Game *game = new Game(bounds, 0, 0, '9', 'Z', false, false);
        game->addPlayer(new Player(BLACK, "Bjarne"));
        return game;
    }

    string readFile(string filename) {
        ifstream t(filename.c_str());
        string str;

        t.seekg(0, ios::end);
        str.reserve(t.tellg());
        t.seekg(0, ios::beg);

        str.assign((istreambuf_iterator<char>(t)),
                   istreambuf_iterator<char>());

        return str;
    }
    string getCommands(string cmdfile) {
        string filename = "tests/plainfiles/" + cmdfile + ".txt";
        return readFile(filename);
    }
    string getExpected(string cmdfile) {
        string filename = "tests/plainfiles/" + cmdfile + ".ref";
        return readFile(filename);
    }
};
