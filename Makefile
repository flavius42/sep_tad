# files linked to the test runner (usually every .o file having a corresponding .cpp file, except main.cpp)
TESTABLE=main.o Action.o Command.o Exception.o Game.o Player.o Quit.o Status.o View.o Helpers.o \
				 SetName.o Save.o Move.o Draw.o MoveAction.o CastlingAction.o ChessPiece.o \
                 Knight.o Queen.o Bishop.o King.o Pawn.o Rook.o Chess.o Point.o Location.o Configuration.o History.o HistoryCommand.o

# files formatted by astyle
DELIVERABLES=main.cpp Color.h Action.h Action.cpp Command.h Command.cpp Exception.h Exception.cpp \
    Game.h Game.cpp Piece.h Player.h Player.cpp Point.h Point.cpp Quit.h Quit.cpp Status.h Status.cpp View.h View.cpp \
		Helpers.h Helpers.cpp SetName.h SetName.cpp Save.h Save.cpp Move.h Move.cpp Draw.h Draw.cpp \
		MoveAction.h MoveAction.cpp CastlingAction.h CastlingAction.cpp \
        ChessPiece.h ChessPiece.cpp Knight.cpp Queen.cpp Bishop.cpp King.cpp Pawn.cpp Rook.cpp \
        Knight.h Queen.h Bishop.h King.h Pawn.h Rook.h Chess.h Chess.cpp Location.cpp Location.h \
        Configuration.cpp Configuration.h History.cpp History.h HistoryCommand.cpp HistoryCommand.h
# deliverable name
ASS=ass3
# compile flags for both the executable and the test suite
CXXFLAGS=-Wall -g3

# you shouldn't need to change anything below
CXX=g++
RUNNER_SRC=runner.cpp
VALGRIND_OPTS=--error-exitcode=1 --leak-check=summary
RUNNER=runner
TESTTOOL=./cxxtest/bin/cxxtestgen
OBJS=$(TESTABLE)
TESTS=$(wildcard tests/*.h)

$(ASS): $(OBJS)
	$(CXX) $(CXXFLAGS) $(OBJS) -o $(ASS)

%.o: src/%.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

.PHONY: tests clean

tests: $(RUNNER)
	valgrind $(VALGRIND_OPTS) ./$(RUNNER)



$(RUNNER): $(TESTABLE)
	$(TESTTOOL) --runner=XUnitPrinter -o $(RUNNER_SRC) tests/*.h
	$(CXX) $(CXXFLAGS) -o $(RUNNER) $(TESTABLE) $(RUNNER_SRC) -I./cxxtest -I.

format: 
	cd src; astyle -A1 -j -c -s2 -p -k3 -Y $(DELIVERABLES)

deliverable: 
	head -n `grep -n -B 1 ".PHONY" Makefile | sed -n 's/^\([0-9]*\).*/\1/p;q;'` Makefile > src/Makefile
	sed -i 's/src\///' src/Makefile
	tar -czf $(ASS).tar.gz -C src $(DELIVERABLES) Makefile
	rm -f src/Makefile

clean:
	rm -rf *.o *.gch $(ASS) $(RUNNER_SRC) $(RUNNER) $(ASS).tar.gz

maurer: 
	cp $(ASS) ass3_test/
	cd ass3_test/
	python sep_ass3_test.py
